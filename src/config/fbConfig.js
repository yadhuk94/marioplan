import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyC_2W7PXe8bCpNFcFt6wU3BLRvkdmiBAws",
  authDomain: "yadhu-marioplan.firebaseapp.com",
  databaseURL: "https://yadhu-marioplan.firebaseio.com",
  projectId: "yadhu-marioplan",
  storageBucket: "yadhu-marioplan.appspot.com",
  messagingSenderId: "988167880134",
  appId: "1:988167880134:web:9d507f71a53abceec506c7",
  measurementId: "G-E7C8SN2KWS",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;
